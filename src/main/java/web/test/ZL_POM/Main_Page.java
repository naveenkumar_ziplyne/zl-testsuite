package web.test.ZL_POM;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.ZL.utils.CommonUtils;
import com.ZL.webdriverbase.AppPage;

public class Main_Page extends AppPage {

	@FindBy(css = "input#userid")
	private WebElement oracleUserName;

	@FindBy(css = "input#password")
	private WebElement oraclePassoword;

	@FindBy(css = "button#btnActive")
	private WebElement oracleLoginBtn;

	@FindBy(xpath = "//button[.='Skip']")
	private WebElement skipButton;

	public void loginOracle(String user, String pass) {
		clearAndType(oracleUserName, user);
		clearAndType(oraclePassoword, pass);
		clickAndWaitForPageLoadComplete(oracleLoginBtn);
		if (isElementPresent(By.xpath("//button[.='Skip']")))
			clickAndWaitForPageLoadComplete(skipButton);
	}

	@FindBy(css = "div#ziplyne-creator")
	private WebElement ziplyneMain;

	@FindBy(css = "div#ziplyne-branching")
	private WebElement ziplyneMain2;

	private static WebElement getShadowRoot(WebDriver driver, WebElement shadowHost) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
		return (WebElement) js.executeScript("return arguments[0].shadowRoot", shadowHost);
	}

	public static WebElement getShadowElementCSS(WebDriver driver, WebElement shadowHost, String cssOfShadowElement) {
		WebElement shardowRoot = getShadowRoot(driver, shadowHost);
		return shardowRoot.findElement(By.cssSelector(cssOfShadowElement));
	}

	public static WebElement getShadowElementXPath(WebDriver driver, WebElement shadowHost, String cssOfShadowElement) {
		WebElement shardowRoot = getShadowRoot(driver, shadowHost);
		return shardowRoot.findElement(By.xpath(cssOfShadowElement));
	}

	public void clickZipLogo() {
		sleep(3000);
		WebElement ziplyneLogo = getShadowElementCSS(getDriver(), ziplyneMain, "div#ziplyne-button");
		click(ziplyneLogo);
		sleep(6000);
	}

	public void zipSignIn(String userName, String password) {
		WebElement ziplyneUserName = getShadowElementCSS(getDriver(), ziplyneMain, "input[type='email']");
		WebElement ziplynePassword = getShadowElementCSS(getDriver(), ziplyneMain, "input[type='password']");
		WebElement ziplyneSignInButton = getShadowElementCSS(getDriver(), ziplyneMain,
				"span[class*='SignInIcon'] path");

		clearAndType(ziplyneUserName, "apimonitor@ziplyne.com");
		clearAndType(ziplynePassword, "apimonitor");
		clickAndWaitForPageLoadComplete(ziplyneSignInButton);
		sleep(6000);

	}

	public void zipLogo() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain,
				"img[src*='src/assets/img/zIcon.png']");
		waitForElementToAppear(ziplyneSignOut);
		sleep(6000);
	}

	public void zipSignOut() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain, "div[title*='Logout']");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(6000);
	}

	public void clickBack() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain, "span[title*='Back']");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(6000);
	}

	public void clickViewZips() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain,
				"div.style__Flex-sc-14m7bqr-0 > button:nth-of-type(2)");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(3000);
	}

	public void searchZipWithName(String name) {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain, "input[placeholder='Search Zip']");
		clearAndType(ziplyneSignOut, name);
		sleep(3000);
	}

	public void editZip(String name, String description) {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain,
				"span.style__ZipDetails-sc-33nv7u-1.ipJdmi");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(2000);
		WebElement zipName = getShadowElementCSS(getDriver(), ziplyneMain, "input[placeholder*='Enter Zip Name']");
		clearAndType(zipName, name);

		WebElement zipDescription = getShadowElementCSS(getDriver(), ziplyneMain, "textarea.TextArea-s6v9ft-0.khCCgl");
		clearAndType(zipDescription, description);
		sleep(1000);

		WebElement zipUpdateButton = getShadowElementCSS(getDriver(), ziplyneMain, "button.Button-sc-1iv6mx5-0");
		clickAndWaitForPageLoadComplete(zipUpdateButton);

		sleep(3000);
	}

	public void addStepInEditZip() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain,
				"a.style__Link-sc-1zu7v1-5:nth-of-type(1)");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(2000);
	}

	public void stopInEditZip() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain,
				"a.style__Link-sc-1zu7v1-5:nth-of-type(2)");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(2000);
		WebElement zipName = getShadowElementCSS(getDriver(), ziplyneMain, "input[placeholder*='Enter Zip Name']");
		clickAndWaitForPageLoadComplete(zipName);

		WebElement zipDescription = getShadowElementCSS(getDriver(), ziplyneMain, "textarea.TextArea-s6v9ft-0.khCCgl");
		clickAndWaitForPageLoadComplete(zipDescription);
		sleep(1000);

		WebElement zipUpdateButton = getShadowElementCSS(getDriver(), ziplyneMain, "button.Button-sc-1iv6mx5-0");
		clickAndWaitForPageLoadComplete(zipUpdateButton);

		sleep(3000);
	}

	public void saveInEditZip() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain,
				"a.style__Link-sc-1zu7v1-5:nth-of-type(3)");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(2000);
	}

	public void playInEditZip() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain,
				"a.style__Link-sc-1zu7v1-5:nth-of-type(4)");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(2000);
		WebElement zipName = getShadowElementCSS(getDriver(), ziplyneMain, "input[placeholder*='Enter Zip Name']");
		clickAndWaitForPageLoadComplete(zipName);

		WebElement zipDescription = getShadowElementCSS(getDriver(), ziplyneMain, "textarea.TextArea-s6v9ft-0.khCCgl");
		clickAndWaitForPageLoadComplete(zipDescription);
		sleep(1000);

		WebElement zipUpdateButton = getShadowElementCSS(getDriver(), ziplyneMain, "button.Button-sc-1iv6mx5-0");
		clickAndWaitForPageLoadComplete(zipUpdateButton);

		sleep(3000);
	}

	public void editZipSteps() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain, "span[title*='Edit Zip']");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(2000);
	}

	public void editZipStepSettings() {
		WebElement zipSettings = getShadowElementCSS(getDriver(), ziplyneMain, "div[title*='Setting']");
		clickAndWaitForPageLoadComplete(zipSettings);
		sleep(3000);
	}

	public void editZipAddBackdrop() {
		WebElement zipSettings = getShadowElementCSS(getDriver(), ziplyneMain,
				"span.Checkbox__StyledSpan-sc-1n8sz3a-2");
		clickAndWaitForPageLoadComplete(zipSettings);
		sleep(3000);
	}

	public void editZipAddPulse() {
		WebElement zipSettings = getShadowElementCSS(getDriver(), ziplyneMain,
				"label.Checkbox__Container-sc-1n8sz3a-0 + label");
		clickAndWaitForPageLoadComplete(zipSettings);
		sleep(3000);
	}

	public void goToTriggersTab() {
		WebElement zipTriggersTab = getShadowElementCSS(getDriver(), ziplyneMain,
				"div.style__Menu-sc-1h3crf2-1:nth-of-type(2)");
		clickAndWaitForPageLoadComplete(zipTriggersTab);
		sleep(1000);
	}

	public void goToUITab() {
		WebElement zipTriggersTab = getShadowElementCSS(getDriver(), ziplyneMain,
				"div.style__Menu-sc-1h3crf2-1:nth-of-type(3)");
		clickAndWaitForPageLoadComplete(zipTriggersTab);
		sleep(1000);
	}

	public void clickHideNextButton() {
		WebElement zipTriggersTab = getShadowElementCSS(getDriver(), ziplyneMain,
				"label.Checkbox__Container-sc-1n8sz3a-0 span");
		clickAndWaitForPageLoadComplete(zipTriggersTab);
		sleep(1000);
	}

	public void clickGeneralAccounting() {
		sleep(5000);
		WebElement gAccounting = getDriver().findElement(By.xpath("//a[@id = 'groupNode_general_accounting']"));
		clickAndWaitForPageLoadComplete(gAccounting);
	}

	public void hoverGeneralAccounting() {
		sleep(5000);
		WebElement gAccounting = getDriver().findElement(By.xpath("//a[@id = 'groupNode_general_accounting']"));
		hoverOnElement(gAccounting);
	}

	public void clickPlay() {
		WebElement zipTriggerClickCheckBox = getShadowElementCSS(getDriver(), ziplyneMain,
				"a.style__Link-sc-1zu7v1-5:nth-of-type(4)");
		zipTriggerClickCheckBox.click();
		sleep(3000);
	}

	public void clickStop() {
		WebElement zipTriggerClickCheckBox = getShadowElementCSS(getDriver(), ziplyneMain,
				"a.style__Link-sc-1zu7v1-5:nth-of-type(2)");
		zipTriggerClickCheckBox.click();
		sleep(3000);
	}

	public void selectClickTrigger() {
		WebElement zipTriggerClickCheckBox = getShadowElementCSS(getDriver(), ziplyneMain,
				"span.Checkbox__StyledSpan-sc-1n8sz3a-2:nth-of-type(1)");
		zipTriggerClickCheckBox.click();
		sleep(3000);
	}

	public void selectHoverTrigger() {
		WebElement zipTriggerClickCheckBox = getShadowElementCSS(getDriver(), ziplyneMain,
				"div#zip-steps div:nth-of-type(5) label");
		zipTriggerClickCheckBox.click();
		sleep(3000);
	}

	public void selectTypingTrigger() {
		WebElement zipTriggerClickCheckBox = getShadowElementCSS(getDriver(), ziplyneMain,
				"div#zip-steps div:nth-of-type(6) label");
		zipTriggerClickCheckBox.click();
		sleep(3000);
	}

	public void selectInvisibleTrigger() {
		WebElement zipTriggerClickCheckBox = getShadowElementCSS(getDriver(), ziplyneMain,
				"div#zip-steps div:nth-of-type(7) label");
		zipTriggerClickCheckBox.click();
		sleep(3000);
	}

	public void selectElementHideTrigger() {
		WebElement zipTriggerClickCheckBox = getShadowElementCSS(getDriver(), ziplyneMain,
				"div#zip-steps div:nth-of-type(8) label");
		zipTriggerClickCheckBox.click();
		sleep(3000);
	}

	public void selectShowHideTrigger() {
		WebElement zipTriggerClickCheckBox = getShadowElementCSS(getDriver(), ziplyneMain,
				"div#zip-steps div:nth-of-type(9) label");
		zipTriggerClickCheckBox.click();
		sleep(3000);
	}

	public void selectMatchInnerText() {
		WebElement zipTriggerClickCheckBox = getShadowElementCSS(getDriver(), ziplyneMain,
				"#zip-steps > div.Accordion__AccordionInnerPanel-sc-15wkt4l-1.gOROuJ > div.style__Checkboxs-sc-1h3crf2-9.ijlLIV");
		zipTriggerClickCheckBox.click();
		sleep(3000);
	}

	public void clickDraftZips() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain,
				"a.style__Link-sc-33nv7u-4:nth-of-type(2)");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(3000);
	}

	public void clickTestZips() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain,
				"a.style__Link-sc-33nv7u-4:nth-of-type(3)");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(3000);
	}

	public void clickLiveZips() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain,
				"a.style__Link-sc-33nv7u-4:nth-of-type(4)");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(3000);
	}

	public void clickArchivedZips() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain,
				"a.style__Link-sc-33nv7u-4:nth-of-type(5)");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(3000);
	}

	public void cloneZips() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain, "div[title*='Clone']");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(3000);

		WebElement selectDD = getShadowElementCSS(getDriver(), ziplyneMain, "select");
		clickAndWaitForPageLoadComplete(selectDD);
		sleep(3000);

		WebElement selectValue = getShadowElementCSS(getDriver(), ziplyneMain, "option:nth-of-type(2)");
		clickAndWaitForPageLoadComplete(selectValue);
		sleep(3000);
		String name = CommonUtils.getCurrentTimeString();

		WebElement enterName = getShadowElementCSS(getDriver(), ziplyneMain, "input.Input-byydkh-0");
		clearAndType(enterName, "N" + name);
		sleep(3000);

		WebElement enterURL = getShadowElementCSS(getDriver(), ziplyneMain, "input[placeholder*='Clone URL']");
		clearAndType(enterURL, "N" + name);
		sleep(3000);

		WebElement buttonn = getShadowElementCSS(getDriver(), ziplyneMain, "button.Button-sc-1iv6mx5-0");
		clickAndWaitForPageLoadComplete(buttonn);
		sleep(3000);

	}

	public void zipBranchingCreate() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain, "span[title*='Branching']");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(3000);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement selectDD = (WebElement) js.executeScript(
				"return document.querySelector('#ziplyne-branching').shadowRoot.querySelector('svg > g > g > circle')");
		selectDD.click();
//		WebElement selectDD = getShadowElementXPath(getDriver(), ziplyneMain, "svg > g");
//		clickAndWaitForPageLoadComplete(selectDD);
		sleep(3000);

		WebElement selectValue = getShadowElementCSS(getDriver(), ziplyneMain2, "button#accordion > div");
		clickAndWaitForPageLoadComplete(selectValue);
		sleep(3000);

		WebElement enterName = getShadowElementCSS(getDriver(), ziplyneMain2, "input.Input-byydkh-0");
		clearAndType(enterName, "journal");
		sleep(3000);

		WebElement selectConnect = getShadowElementCSS(getDriver(), ziplyneMain2,
				"div#list-zip label.Checkbox__Container-sc-1n8sz3a-0");
		clickAndWaitForPageLoadComplete(selectConnect);
		sleep(3000);

		WebElement selectConnectSave = getShadowElementCSS(getDriver(), ziplyneMain2, "button.Button-sc-1iv6mx5-0");
		clickAndWaitForPageLoadComplete(selectConnectSave);
		sleep(3000);

	}

	public void zipBranchingRemove() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain, "span[title*='Branching']");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(3000);

		WebElement selectDD = getShadowElementCSS(getDriver(), ziplyneMain2, "svg > g > g > circle");
		clickAndWaitForPageLoadComplete(selectDD);
		sleep(3000);

		WebElement selectValue = getShadowElementCSS(getDriver(), ziplyneMain2, "button#accordion");
		clickAndWaitForPageLoadComplete(selectValue);
		sleep(3000);
		String name = CommonUtils.getCurrentTimeString();

		WebElement enterName = getShadowElementCSS(getDriver(), ziplyneMain2, "input.Input-byydkh-0");
		clearAndType(enterName, "journal");
		sleep(3000);

		WebElement selectConnect = getShadowElementCSS(getDriver(), ziplyneMain2,
				"div#list-zip label.Checkbox__Container-sc-1n8sz3a-0");
		clickAndWaitForPageLoadComplete(selectConnect);
		sleep(3000);

		WebElement selectConnectSave = getShadowElementCSS(getDriver(), ziplyneMain2, "button.Button-sc-1iv6mx5-0");
		clickAndWaitForPageLoadComplete(selectConnectSave);
		sleep(3000);

	}

	public void closeZipBranching() {
		WebElement ziplyneSignOut = getShadowElementCSS(getDriver(), ziplyneMain2,
				"div.style__BranchingTitle-sc-1cmm1yf-1 > span");
		clickAndWaitForPageLoadComplete(ziplyneSignOut);
		sleep(3000);
	}

	public Main_Page(WebDriver driver) {
		super(driver);
		waitForPageLoadComplete();
	}

	public Main_Page(WebDriver driver, String url) {
		super(driver);
		this.driver.get(url);
		waitForPageLoadComplete();
	}

	public Main_Page loadMainPage(String url) {
		getDriver().get(url);
		return new Main_Page(getDriver());
	}

}
