package web.test.ZL_testsuite;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import web.test.ZL_testsuite.basetest.TestMain;
import web.test.ZL_testsuite.contants.TestConstant;

public class Creator_Test extends TestMain {

	@Test(description = "Verify SignIn", priority = 0)
	public void testSignIn() {
		try {
			loadMainPage();
			mainPage.loginOracle(TestConstant.ORACLE_USERNAME, TestConstant.ORACLE_PASSWORD);
			mainPage.clickZipLogo();
			mainPage.zipSignIn("", "");
			log("Good");
		} catch (Exception e) {
			e.printStackTrace();
			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
		}
	}

	@Test(description = "Verify ViewZips and search", priority = 1)
	public void testViewZips() {
		try {
			mainPage.clickViewZips();
		} catch (Exception e) {
			e.printStackTrace();
			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
		}
	}
//
//	@Test(description = "Verify ViewZips and search", priority = 2)
//	public void testZipsAddPulseAndBackdrop() {
//		try {
//			mainPage.editZipSteps();
//			mainPage.editZipStepSettings();
//			mainPage.editZipAddBackdrop();
//			mainPage.editZipAddPulse();
//		} catch (Exception e) {
//			e.printStackTrace();
//			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
//		}
//	}
//
//	@Test(description = "Verify ViewZips and search", priority = 3, dependsOnMethods = {
//			"testZipsAddPulseAndBackdrop" })
//	public void testZipSettingUI() {
//		try {
//
//			mainPage.goToUITab();
//			mainPage.clickHideNextButton();
//			mainPage.clickHideNextButton();
//		} catch (Exception e) {
//			e.printStackTrace();
//			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
//		}
//	}
//
//	@Test(description = "Verify ViewZips and search", priority = 3, dependsOnMethods = { "testZipSettingUI" })
//	public void testZipSettingTrigger() {
//		try {
//			mainPage.goToTriggersTab();
//			mainPage.selectClickTrigger();
//			mainPage.clickBack();
//			mainPage.clickPlay();
//			mainPage.clickGeneralAccounting();
//			mainPage.clickZipLogo();
//			mainPage.clickStop();
//
//			mainPage.editZipStepSettings();
//			mainPage.goToTriggersTab();
//			mainPage.selectHoverTrigger();
//			mainPage.clickBack();
//			mainPage.clickPlay();
//			mainPage.clickGeneralAccounting();
//			mainPage.clickZipLogo();
//			mainPage.clickStop();
//
//			mainPage.editZipStepSettings();
//			mainPage.goToTriggersTab();
//			mainPage.selectTypingTrigger();
//			mainPage.clickBack();
//
//			mainPage.editZipStepSettings();
//			mainPage.goToTriggersTab();
//			mainPage.selectInvisibleTrigger();
//			mainPage.clickBack();
//
//			mainPage.editZipStepSettings();
//			mainPage.goToTriggersTab();
//			mainPage.selectElementHideTrigger();
//			mainPage.clickBack();
//
//			mainPage.editZipStepSettings();
//			mainPage.goToTriggersTab();
//			mainPage.selectShowHideTrigger();
//			mainPage.clickBack();
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
//		}
//	}
//
//	@Test(description = "Verify ViewZips and search", priority = 3, dependsOnMethods = { "testZipSettingTrigger" })
//	public void testZipAdvancedSettings() {
//		try {
//			mainPage.editZipStepSettings();
//			mainPage.selectMatchInnerText();
//			mainPage.clickBack();
//			mainPage.clickBack();
//		} catch (Exception e) {
//			e.printStackTrace();
//			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
//		}
//	}
//
//	@Test(description = "Verify ViewZips and search", priority = 4)
//	public void testSearchZips() {
//		try {
//			mainPage.searchZipWithName("Create a journal");
//		} catch (Exception e) {
//			e.printStackTrace();
//			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
//		}
//	}
//
//	@Test(description = "Verify Editing of Zips", priority = 4, dependsOnMethods = { "testSearchZips" })
//	public void testEditZips() {
//		try {
//			mainPage.editZip("Creating a journal", "zip description");
//			mainPage.clickBack();
//		} catch (Exception e) {
//			e.printStackTrace();
//			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
//		}
//	}

//	@Test(description = "Verify Draft Zips and search", priority = 4, dependsOnMethods = { "testEditZips" })
//	public void testDraftZips() {
//		try {
//			mainPage.clickDraftZips();
//			mainPage.searchZipWithName("Creating a journal");
//		} catch (Exception e) {
//			e.printStackTrace();
//			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
//		}
//	}
//
//	@Test(description = "Verify Test Zips and search", priority = 4, dependsOnMethods = { "testDraftZips" })
//	public void testTestZips() {
//		try {
//			mainPage.clickTestZips();
//			mainPage.searchZipWithName("Creating a journal");
//		} catch (Exception e) {
//			e.printStackTrace();
//			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
//		}
//	}
//
//	@Test(description = "Verify Live Zips  and search", priority = 4, dependsOnMethods = { "testTestZips" })
//	public void testLiveZips() {
//		try {
//			mainPage.clickLiveZips();
//			mainPage.searchZipWithName("Creating a journal");
//		} catch (Exception e) {
//			e.printStackTrace();
//			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
//		}
//	}
//
//	@Test(description = "Verify Archive Zips and search", priority = 4, dependsOnMethods = { "testLiveZips" })
//	public void testArchivedZips() {
//		try {
//			mainPage.clickArchivedZips();
//			mainPage.searchZipWithName("Creating a journal");
//		} catch (Exception e) {
//			e.printStackTrace();
//			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
//		}
//	}
//
//	@Test(description = "Verify Archive Zips and search", priority = 5)
//	public void testCloneZips() {
//		try {
//			mainPage.cloneZips();
//		} catch (Exception e) {
//			e.printStackTrace();
//			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
//		}
//	}

	@Test(description = "Verify Archive Zips and search", priority = 6)
	public void testZipFlow() {
		try {
			mainPage.editZipSteps();
			mainPage.editZipStepSettings();
			mainPage.zipBranchingCreate();
			mainPage.closeZipBranching();
			mainPage.zipBranchingRemove();
			mainPage.closeZipBranching();
		} catch (Exception e) {
			e.printStackTrace();
			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
		}
	}

	@Test(description = "Verify Back Operation", priority = 7)
	public void testBackButton() {
		try {
			mainPage.clickBack();
		} catch (Exception e) {
			e.printStackTrace();
			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
		}
	}

	@Test(description = "Verify Ziplyne Logo", priority = 12)
	public void testZiplyneLogo() {
		try {
			mainPage.zipLogo();
		} catch (Exception e) {
			e.printStackTrace();
			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
		}
	}

	@Test(description = "Verify SignOut", priority = 12)
	public void testSignOut() {
		try {
			mainPage.zipSignOut();
		} catch (Exception e) {
			e.printStackTrace();
			AssertJUnit.fail("Exception happened in deleteClass::" + ExceptionUtils.getRootCauseStackTrace(e));
		}
	}
}
