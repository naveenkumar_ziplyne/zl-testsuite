package web.test.ZL_testsuite.basetest;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import com.ZL.utils.CSVParserUtils;
import com.ZL.utils.CommonUtils;
import com.ZL.utils.NadaEMailService;
import com.ZL.utils.ImageComparison.TakeScreenshot;
import com.ZL.utils.ImageComparison.TakeScreenshotUtils;
import com.ZL.webdriverbase.AppTest;
import com.aventstack.extentreports.Status;

import web.test.ZL_POM.Main_Page;
import web.test.ZL_testsuite.contants.TestConstant;

public class TestMain extends AppTest {

	public static Logger logger = LoggerFactory.getLogger(TestMain.class);

	public CommonUtils utils = new CommonUtils();
	public CSVParserUtils csvParser = new CSVParserUtils();
	public static NadaEMailService nadaMailService = new NadaEMailService();

	// Main Pages
	public web.test.ZL_POM.Main_Page mainPage;

	@BeforeTest
	public void beforeTest() {
		spark.config().enableTimeline(true);
		spark.config().setDocumentTitle("Ziplyne Test Report");
		spark.config().setReportName("Ziplyne Automation Test Report");

		extent.setSystemInfo("Environment", TestConstant.ENVIRONMENT);
		extent.setSystemInfo("URL", TestConstant.ORACLE_BASE_PAGE_URL);
		extent.setSystemInfo("Browser", "Chrome");
	}

	@BeforeClass
	public void beforeClass() {
	}

	public void loadMainPage() {
		if (hasDriver() && mainPage != null) {
			mainPage = mainPage.loadMainPage(TestConstant.ORACLE_BASE_PAGE_URL);
		}
		if (!hasDriver() || mainPage == null) {
			getDriver().get(TestConstant.ORACLE_BASE_PAGE_URL);
			mainPage = new Main_Page(getDriver());
		}
		getDriver().manage().window().maximize();
	}

	public static void log(String message) {
		test.log(Status.INFO, message);
		logger.info(message);
	}

	public void takeScreenShot(String fileName) {
		WebDriver driver = getDriver();
		if (driver != null) {
			TakeScreenshot ts = new TakeScreenshotUtils(false, "", "", false);
			ts.captureScreenShot(driver, fileName);
			utils.captureFullBrowserScreenShot(fileName, driver);
		} else {
			logger.info("Couldn't take screenshot.. No driver found.");
		}

	}

	public void takeScreenShot() {
		WebDriver driver = getDriver();
		if (driver != null) {
			String fileName = null;
			StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();
			fileName = fileName + stackTraceElements[2].getMethodName() + ".png";
			TakeScreenshot ts = new TakeScreenshotUtils(false, "", "", false);
			ts.captureScreenShot(driver, fileName);
			fileName = fileName.replace(".png", "");
			utils.captureFullBrowserScreenShot(fileName, driver);
		} else {
			logger.info("Couldn't take screenshot.. No driver found.");
		}

	}

	public void afterTest() {
		stopDriver();
	}

	public void fail(String message) {
		String additionalDetails = "test failed";
		Assert.fail(message + additionalDetails);
		// takeScreenShot(WebDriverConstants.PATH_TO_BROWSER_SCREENSHOT + message +
		// ".png");
	}

	public void fail(String message, Exception e) {
		if (e instanceof SkipException)
			throw new SkipException(e.getMessage());
		if (e instanceof UnhandledAlertException) {
			try {
				Alert a = getDriver().switchTo().alert();
				String alertMessage = a.getText();
				a.accept();
				message += " \nMessage from unhandled alert box: \n" + alertMessage;
			} catch (NoAlertPresentException ex) {
			}
		}
		fail(message + "\nMessage from Exception:\n" + ExceptionUtils.getRootCauseStackTrace(e));
	}
}
